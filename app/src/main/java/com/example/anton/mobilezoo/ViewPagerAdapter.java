package com.example.anton.mobilezoo;

/**
 * Created by anton on 13.04.2016.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.anton.mobilezoo.ui.fragments.TabFragment;
import com.example.anton.mobilezoo.ui.fragments.TabList;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence titles[];
    private int numbOfTabs;


    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.titles = mTitles;
        this.numbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {

        TabFragment tab = new TabFragment();
        TabList tabL = new TabList();
        switch (position) {
            case 0:
                tabL.setLText("Hey");
                return tabL;
            case 1:
                tab.setText("to be continued...");
                break;


        }
        return tab;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return numbOfTabs;
    }
}