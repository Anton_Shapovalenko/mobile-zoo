package com.example.anton.mobilezoo.db;

/**
 * Created by anton on 24.04.2016.
 */
public class Messeges {
    public static final String TABLE_NAME = "Messeges";
    public static final String _ID = "messeges_id";
    public static final String TABLE = "tables";
    public static final String SEAT = "seat";
    public static final String TYPE = "type";
    public static final String TIMESTAMP = "time_stamp";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TABLE + " INT,"
            + SEAT + " VARCHAR(255),"
            + TYPE + " VARCHAR(255),"
            + TIMESTAMP + " INTEGER);";
    static final String AUTHORITY = "com.example.anton.Mobile_zoo.DB.Messeges";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;
    public static String table;
}
