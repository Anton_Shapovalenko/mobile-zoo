package com.example.anton.mobilezoo.ui.fragments;

/**
 * Created by anton on 13.04.2016.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anton.mobilezoo.R;


public class TabFragment extends Fragment {
    private String textS;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.tab_1, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        TextView text = (TextView) v.findViewById(R.id.textView);
        text.setText(textS);
    }

    public void setText(String textNew) {
        textS = textNew;
    }


}