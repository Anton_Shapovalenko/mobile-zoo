package com.example.anton.mobilezoo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.example.anton.mobilezoo.data.MenuItemObj;
import com.example.anton.mobilezoo.data.MessegesObj;
import com.example.anton.mobilezoo.data.OrderItemObj;
import com.example.anton.mobilezoo.data.OrderObj;

import java.util.ArrayList;

/**
 * Created by anton on 19.04.2016.
 */
public class DB extends SQLiteOpenHelper implements BaseColumns {
    private static final String DATABASE_NAME = "Mobile_zoo";
    private static final int DATABASE_VERSION = 1;

    public DB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Messeges.SQL_CREATE_TABLE);
        db.execSQL(Table.SQL_CREATE_TABLE);
        db.execSQL(TableMenuItem.SQL_CREATE_TABLE);
        db.execSQL(TableOrder.SQL_CREATE_TABLE);
        db.execSQL(TableOrderItem.SQL_CREATE_TABLE);
    }


    public void insertOrderData(ArrayList<OrderObj> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            values.put(TableOrder.TABLE, list.get(i).getTable());
            values.put(TableOrder.SEAT, list.get(i).getSeat());
            values.put(TableOrder.TOTAL, list.get(i).getTotal());
            values.put(TableOrder.STATUS, list.get(i).getStatus());
            values.put(TableOrder.MESSAGES, list.get(i).getMessages());
            writableDatabase.insert(TableOrder.TABLE_NAME, null, values);
        }
    }

    public void insertMenuItemData(ArrayList<MenuItemObj> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            values.put(TableMenuItem.NAME, list.get(i).getName());
            values.put(TableMenuItem.PRICE, list.get(i).getPrice());
            values.put(TableMenuItem.DESCRIPTION, list.get(i).getDescription());
            values.put(TableMenuItem._ID, list.get(i).getId());

            writableDatabase.insert(TableMenuItem.TABLE_NAME, null, values);
        }
    }

    public void insertOrderItemData(ArrayList<OrderItemObj> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            values.put(TableOrderItem.NAME, list.get(i).getName());
            values.put(TableOrderItem.PRICE, list.get(i).getPrice());
            values.put(TableOrderItem.STATUS, list.get(i).getStatus());
            values.put(TableOrderItem.TABLE_ID, list.get(i).getTableId());
            values.put(TableOrderItem._ID, list.get(i).getId());
            writableDatabase.insert(TableOrderItem.TABLE_NAME, null, values);
        }
    }

    public void insertTableTableData(ArrayList<String> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Table.STATUS, list.get(0));
        values.put(Table.SEATS, list.get(1));
        values.put(Table.NUMBER, list.get(2));
        writableDatabase.insert(Table.TABLE_NAME, null, values);
    }

    public void insertMessegeseData(ArrayList<MessegesObj> list) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < list.size(); i++) {
            values.put(Messeges.TABLE, list.get(i).getTable());
            values.put(Messeges.SEAT, list.get(i).getSeat());
            values.put(Messeges.TYPE, list.get(i).getType());
            values.put(Messeges.TIMESTAMP, list.get(i).getTimeStamp());
            writableDatabase.insert(Messeges.TABLE_NAME, null, values);
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}