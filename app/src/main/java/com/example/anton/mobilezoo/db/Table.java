package com.example.anton.mobilezoo.db;

/**
 * Created by anton on 20.04.2016.
 */
public class Table {

    public static final String TABLE_NAME = "Tables";
    public static final int DATABASE_VERSION = 1;
    public static final String _ID = "table_id";
    public static final String STATUS = "status";
    public static final String SEATS = "seats";
    public static final String NUMBER = "number";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + STATUS + " INT,"
            + SEATS + " VARCHAR(255),"
            + NUMBER + " INT);";
    static final String AUTHORITY = "com.example.anton.Mobile_zoo.DB.TableMenuItem";
}
