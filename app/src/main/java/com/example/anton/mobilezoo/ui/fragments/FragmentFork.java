package com.example.anton.mobilezoo.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.MessegesObj;

import java.text.SimpleDateFormat;

/**
 * Created by anton on 26.04.2016.
 */
public class FragmentFork extends Fragment {
    private MessegesObj obj;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fork, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        if (getArguments() != null) {
            obj = getArguments().getParcelable("key");
        }
        TextView time = (TextView) v.findViewById(R.id.times);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateString = formatter.format(obj.getTimeStamp());
        time.setText(dateString);

        TextView tables = (TextView) v.findViewById(R.id.tables);
        tables.setText(obj.getTable() + obj.getSeat());

        final Button call = (Button) v.findViewById(R.id.button_fork);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }
}
