package com.example.anton.mobilezoo.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.anton.mobilezoo.InformationActivity;
import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.MessegesObj;


/**
 * Created by anton on 17.04.2016.
 */
public class FragmentMessage extends Fragment {

    private MessegesObj obj;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.item_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        final Button button = (Button) v.findViewById(R.id.button);
        if (getArguments() != null) {
            obj = getArguments().getParcelable(InformationActivity.EXTRA_KEY_MESSAGEOBJ);
        }
        Log.d("FragmentMessage TYT:", obj.getTable() + obj.getSeat());
        TextView textView = (TextView) v.findViewById(R.id.message_text);
        String type = obj.getType();
        switch (type) {
            case MessegesObj.MessageType.FORK:
                textView.setText("Принеси приборы столу " + obj.getTable() + obj.getSeat());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        button.setText("Okay");
                        Bundle bundle;
                        bundle = new Bundle();
                        bundle.putParcelable("key", obj);
                        FragmentFork fragmentFork = new FragmentFork();
                        fragmentFork.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.cont_frag, fragmentFork).addToBackStack("MyStack").commit();
                    }
                });
                break;
            case MessegesObj.MessageType.COME:
                textView.setText("Подойди к столу " + obj.getTable() + obj.getSeat());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        button.setText("dd");
                        Bundle bundle;
                        bundle = new Bundle();
                        bundle.putParcelable("key", obj);
                        FragmentCome fragmentCome = new FragmentCome();
                        fragmentCome.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.cont_frag, fragmentCome).addToBackStack("MyStack").commit();
                    }
                });
                break;
            case MessegesObj.MessageType.TAXI:
                textView.setText("Вызви такси столу " + obj.getTable() + obj.getSeat());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        button.setText("Okay");
                        Bundle bundle;
                        bundle = new Bundle();
                        bundle.putParcelable("key", obj);
                        FragmentTaxi fragmentTaxi = new FragmentTaxi();
                        fragmentTaxi.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.cont_frag, fragmentTaxi).addToBackStack("MyStack").commit();
                    }
                });
                break;
            case MessegesObj.MessageType.CHECK:
                textView.setText("Принеси чек столу " + obj.getTable() + obj.getSeat());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        button.setText("Okay");
                        Bundle bundle;
                        bundle = new Bundle();
                        bundle.putParcelable("key", obj);
                        FragmentCheck fragmentCheck = new FragmentCheck();
                        fragmentCheck.setArguments(bundle);
                        getFragmentManager().beginTransaction()
                                .replace(R.id.cont_frag, fragmentCheck)
                                .addToBackStack("myStack")
                                .commit();
                    }
                });
                break;
        }

    }
}
