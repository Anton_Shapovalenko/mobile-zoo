package com.example.anton.mobilezoo.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by anton on 22.04.2016.
 */
public class MessegesObj implements Parcelable {
    private static final String TAG = "MessegesObj";
    public static final Parcelable.Creator<MessegesObj> CREATOR = new Parcelable.Creator<MessegesObj>() {
        // распаковываем объект из Parcel
        public MessegesObj createFromParcel(Parcel in) {
            Log.d(TAG, "createFromParcel");
            return new MessegesObj(in);
        }

        public MessegesObj[] newArray(int size) {
            return new MessegesObj[size];
        }
    };
    private int table;
    private String seat;
    private String type;
    private long timeStamp;

    public MessegesObj(int table, String seat, String type, long timeStamp) {
        this.table = table;
        this.seat = seat;
        this.type = type;
        this.timeStamp = timeStamp;
    }

    // конструктор, считывающий данные из Parcel
    private MessegesObj(Parcel parcel) {
        Log.d(TAG, "MyObject(Parcel parcel)");
        table = parcel.readInt();
        seat = parcel.readString();
        type = parcel.readString();
        timeStamp = parcel.readLong();
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long time) {
        this.timeStamp = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Log.d(TAG, "writeToParcel");
        parcel.writeInt(table);
        parcel.writeString(seat);
        parcel.writeString(type);
        parcel.writeLong(timeStamp);
    }

    public static class MessageType {
        public static final String COME = "come";
        public static final String TAXI = "taxi";
        public static final String CHECK = "check";
        public static final String FORK = "fork";
    }
}
