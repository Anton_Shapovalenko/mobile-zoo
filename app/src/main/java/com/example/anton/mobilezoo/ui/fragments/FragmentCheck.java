package com.example.anton.mobilezoo.ui.fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.MessegesObj;
import com.example.anton.mobilezoo.data.OrderItemObj;
import com.example.anton.mobilezoo.db.DB;
import com.example.anton.mobilezoo.db.TableOrderItem;
import com.example.anton.mobilezoo.ui.BasicActivity;
import com.example.anton.mobilezoo.ui.list.OrderAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by anton on 19.04.2016.
 */
public class FragmentCheck extends Fragment {
    private ArrayList<OrderItemObj> orders = new ArrayList<>();
    private MessegesObj obj;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check, container, false);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);


        if (getArguments() != null) {
            obj = getArguments().getParcelable("key");
        }

        final DB sqh = ((BasicActivity)getActivity()).getDb();
        SQLiteDatabase sqdb = sqh.getReadableDatabase();
        Cursor cursor = getTablesCursor(sqdb, obj);
        fillOrdersList(cursor);
        cursor.close();


        TextView time = (TextView) v.findViewById(R.id.check_time);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateString = formatter.format(obj.getTimeStamp());
        time.setText(dateString);
        TextView table = (TextView) v.findViewById(R.id.check_table);
        table.setText(obj.getTable() + obj.getSeat());


        TextView acct = (TextView) v.findViewById(R.id.check_price);
        float moneyAcct = 0;
        for (int i = 0; i < orders.size(); i++) {
            moneyAcct += orders.get(i).getPrice();
        }
        acct.setText("$" + String.format("%.2f", moneyAcct));

        RecyclerView rv = (RecyclerView) v.findViewById(R.id.rv2);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        OrderAdapter adapter = new OrderAdapter(orders);
        rv.setAdapter(adapter);
        final Button call = (Button) v.findViewById(R.id.check_button);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }


        });
    }

    private void fillOrdersList(Cursor cursor) {
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(TableOrderItem._ID));
            String name = cursor.getString(cursor.getColumnIndex(TableOrderItem.NAME));
            float price = cursor.getFloat(cursor.getColumnIndex(TableOrderItem.PRICE));
            String status = cursor.getString(cursor.getColumnIndex(TableOrderItem.STATUS));
            int tableId = cursor.getInt(cursor.getColumnIndex(TableOrderItem.TABLE_ID));
            OrderItemObj newOrderItem = new OrderItemObj(id, name, price, status, tableId);
            orders.add(newOrderItem);
        }

    }

    private Cursor getTablesCursor(SQLiteDatabase sqdb, MessegesObj obj) {
        return sqdb.query(TableOrderItem.TABLE_NAME, new String[]{TableOrderItem._ID, TableOrderItem.NAME, TableOrderItem.PRICE, TableOrderItem.STATUS, TableOrderItem.TABLE_ID}, TableOrderItem.TABLE_ID + " = ?", new String[]{String.valueOf(obj.getTable())}, null, null, null);
    }
}
