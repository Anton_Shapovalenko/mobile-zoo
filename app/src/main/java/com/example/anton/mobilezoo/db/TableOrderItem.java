package com.example.anton.mobilezoo.db;

/**
 * Created by anton on 19.04.2016.
 */
public class TableOrderItem {
    public static final String TABLE_NAME = "Order_item";
    public static final String _ID = "order_item_id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String STATUS = "status";
    public static final String TABLE_ID = "tables_id";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAME + " VARCHAR(255),"
            + PRICE + " REAL,"
            + STATUS + " VARCHAR(255),"
            + TABLE_ID + " INT);";
    static final String AUTHORITY = "com.example.anton.Mobile_zoo.DB.TableOrderItem";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;

}
