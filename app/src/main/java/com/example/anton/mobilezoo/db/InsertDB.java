package com.example.anton.mobilezoo.db;

import com.example.anton.mobilezoo.data.OrderItemObj;
import com.example.anton.mobilezoo.data.OrderObj;

import java.util.ArrayList;

/**
 * Created by anton on 21.04.2016.
 */
public class InsertDB {

    public static void fillingDB(DB sqh) {

/*
        ArrayList<MenuItemObj> menuItem1 = new ArrayList<>();
        menuItem1.add(new MenuItemObj(1, "Big Mac", 2.1, "Calories - 540"));
        menuItem1.add(new MenuItemObj(2, "Hamburger", 3.3, "Calories - 250"));
        menuItem1.add(new MenuItemObj(3, "Cheeseburger", 1, "Calories - 300"));
        menuItem1.add(new MenuItemObj(4, "McChicken", 2.2, "Calories - 370"));
        menuItem1.add(new MenuItemObj(5, "Famous Fries", 1.15, "Calories - 230"));
        menuItem1.add(new MenuItemObj(6, "Chicken McNuggets", 2.75, "Calories - 190"));
        sqh.insertMenuItemData(menuItem1);*/


        ArrayList<OrderItemObj> orderItem1 = new ArrayList();
        orderItem1.add(new OrderItemObj(1, "Big Mac", 2.1f, "new", 1));
        orderItem1.add(new OrderItemObj(2, "Hamburger", 3.4f, "new", 1));
        orderItem1.add(new OrderItemObj(3, "Cheeseburger", 1f, "new", 1));
        orderItem1.add(new OrderItemObj(4, "McChicken", 2.3f, "new", 5));
        orderItem1.add(new OrderItemObj(5, "Famous Fries", 1.15f, "new", 5));
        orderItem1.add(new OrderItemObj(6, "Chicken McNuggets", 2.75f, "new", 5));
        orderItem1.add(new OrderItemObj(7, "Big Mac", 2.1f, "new", 1));
        orderItem1.add(new OrderItemObj(8, "Cheeseburger", 1f, "new", 4));
        orderItem1.add(new OrderItemObj(9, "Cheeseburger", 1, "new", 5));
        orderItem1.add(new OrderItemObj(10, "McChicken", 2.2f, "new", 1));
        orderItem1.add(new OrderItemObj(11, "Famous Fries", 1.15f, "new", 5));
        sqh.insertOrderItemData(orderItem1);


        ArrayList<OrderObj> orderObjs = new ArrayList<>();
        ArrayList<OrderItemObj> listOrderItemObj1 = new ArrayList<>();
        listOrderItemObj1.add(new OrderItemObj(1, "Big Mac", 2, "new", 1));
        listOrderItemObj1.add(new OrderItemObj(3, "Cheeseburger", 1, "new", 3));
        listOrderItemObj1.add(new OrderItemObj(4, "McChicken", 2, "new", 4));
        orderObjs.add(new OrderObj(1, "A", 5, "open", "Hello", listOrderItemObj1));

        ArrayList<OrderItemObj> listOrderItemObj2 = new ArrayList<>();
        listOrderItemObj2.add(new OrderItemObj(3, "Cheeseburger", 1, "new", 3));
        listOrderItemObj2.add(new OrderItemObj(4, "McChicken", 2, "new", 4));
        listOrderItemObj2.add(new OrderItemObj(6, "Chicken McNuggets", 3, "new", 6));
        orderObjs.add(new OrderObj(2, "D", 6, "open", "Hello2", listOrderItemObj2));

        ArrayList<OrderItemObj> listOrderItemObj3 = new ArrayList<>();
        listOrderItemObj3.add(new OrderItemObj(2, "Hamburger", 3, "new", 2));
        listOrderItemObj3.add(new OrderItemObj(4, "McChicken", 2, "new", 4));
        listOrderItemObj3.add(new OrderItemObj(5, "Famous Fries", 1, "new", 5));
        listOrderItemObj3.add(new OrderItemObj(6, "Chicken McNuggets", 3, "new", 6));
        orderObjs.add(new OrderObj(3, "B", 9, "paid", "Hello3", listOrderItemObj3));

        sqh.insertOrderData(orderObjs);


    }
}
