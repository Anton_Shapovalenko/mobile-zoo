package com.example.anton.mobilezoo.db;

/**
 * Created by anton on 19.04.2016.
 */
public class TableMenuItem {
    public static final String TABLE_NAME = "Menu_item";
    public static final int DATABASE_VERSION = 1;
    public static final String _ID = "menu_item_id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String DESCRIPTION = "description";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAME + " VARCHAR(255),"
            + PRICE + " REAL,"
            + DESCRIPTION + " VARCHAR(255));";
    static final String AUTHORITY = "com.example.anton.Mobile_zoo.DB.TableMenuItem";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;

}
