package com.example.anton.mobilezoo.data;

/**
 * Created by anton on 21.04.2016.
 */
public class OrderItemObj {
    public String name;
    public float price;
    public String status;
    public int tableId;
    public int id;

    public OrderItemObj(int id, String name, float price, String status, int tableId) {
        this.name = name;
        this.price = price;
        this.status = status;
        this.tableId = tableId;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
