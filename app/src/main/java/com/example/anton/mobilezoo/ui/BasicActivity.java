package com.example.anton.mobilezoo.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.anton.mobilezoo.db.DB;

/**
 * Created by anton on 27.04.2016.
 */
public abstract class BasicActivity extends AppCompatActivity {
    private DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new DB(this);
    }

    public DB getDb() {
        return db;
    }
}
