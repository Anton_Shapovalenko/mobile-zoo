package com.example.anton.mobilezoo;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.anton.mobilezoo.data.MessegesObj;
import com.example.anton.mobilezoo.ui.BasicActivity;
import com.example.anton.mobilezoo.ui.fragments.FragmentMessage;

/**
 * Created by anton on 17.04.2016.
 */
public class InformationActivity extends BasicActivity {

    public static String EXTRA_KEY_MESSAGEOBJ = "EXTRA_KEY_MESSAGEOBJ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.second_list_activity);

        FragmentMessage fragmentMessage = new FragmentMessage();


        MessegesObj myObj = (MessegesObj) getIntent().getParcelableExtra(EXTRA_KEY_MESSAGEOBJ);
        Log.d("InformationActivity", myObj.getTable() + myObj.getSeat());

        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_KEY_MESSAGEOBJ, myObj);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentMessage.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.cont_frag, fragmentMessage)
                .commit();

    }


    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
