package com.example.anton.mobilezoo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.anton.mobilezoo.data.MessegesObj;
import com.example.anton.mobilezoo.db.DB;
import com.example.anton.mobilezoo.db.InsertDB;
import com.example.anton.mobilezoo.db.Messeges;
import com.example.anton.mobilezoo.ui.BasicActivity;
import com.example.anton.mobilezoo.ui.slidingTab.SlidingTabLayout;

import java.util.ArrayList;


public class MainActivity extends BasicActivity {

    public static final int HOUR_TO_MILLIS = 60 * 60 * 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        CharSequence Titles[] = {getString(R.string.tab1), getString(R.string.tab2)};
        int numbOfTabs = 2;

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, numbOfTabs);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);


        SlidingTabLayout tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.yellowColor);
            }
        });

        tabs.setViewPager(pager);



        final DB sqh = getDb();
        SQLiteDatabase sqdb = sqh.getWritableDatabase();
        Cursor cursor = getTablesCursor(sqdb);
        if (cursor.getCount() == 0) {
            sqh.insertMessegeseData( initializeData());
            InsertDB.fillingDB(sqh);
        }

    }


    private Cursor getTablesCursor(SQLiteDatabase sqdb) {
        return sqdb.query(Messeges.TABLE_NAME, new String[]{Messeges._ID,
                        Messeges.TABLE, Messeges.SEAT, Messeges.TYPE, Messeges.TIMESTAMP},
                null, null, null, null, null);
    }


    private ArrayList<MessegesObj> initializeData() {
        ArrayList<MessegesObj> table = new ArrayList<>();

        table.add(new MessegesObj(1, "A", MessegesObj.MessageType.CHECK, System.currentTimeMillis()));
        table.add(new MessegesObj(1, "B", MessegesObj.MessageType.TAXI, System.currentTimeMillis() - 2 * HOUR_TO_MILLIS));
        table.add(new MessegesObj(3, "C", MessegesObj.MessageType.FORK, System.currentTimeMillis() - 4 * HOUR_TO_MILLIS));
        table.add(new MessegesObj(5, "D", MessegesObj.MessageType.CHECK, System.currentTimeMillis() - 5 * HOUR_TO_MILLIS));
        table.add(new MessegesObj(4, "A", MessegesObj.MessageType.COME, System.currentTimeMillis() - 2 * HOUR_TO_MILLIS));
        table.add(new MessegesObj(4, "C", MessegesObj.MessageType.COME, System.currentTimeMillis() - 1 * HOUR_TO_MILLIS));

return table;
    }

}
