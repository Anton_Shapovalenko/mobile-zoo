package com.example.anton.mobilezoo.data;


import java.util.ArrayList;

/**
 * Created by anton on 21.04.2016.
 */
public class TableObj {
    public static String status;
    public static String number;
    public static ArrayList<String> listSeats;

    public TableObj(String status, String number, ArrayList<String> listSeats) {
        this.status = status;
        this.number = number;
        this.listSeats = listSeats;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        TableObj.status = status;
    }

    public static String getNumber() {
        return number;
    }

    public static void setNumber(String number) {
        TableObj.number = number;
    }

    public static ArrayList<String> getListSeats() {
        return listSeats;
    }

    public static void setListSeats(ArrayList<String> listSeats) {
        TableObj.listSeats = listSeats;
    }
}
