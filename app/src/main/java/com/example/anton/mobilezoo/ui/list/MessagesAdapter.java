package com.example.anton.mobilezoo.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.MessegesObj;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by anton on 14.04.2016.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder> {


    private List<MessegesObj> messages;

    public MessagesAdapter(List<MessegesObj> table) {
        this.messages = table;
    }


    @Override
    public MessagesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new MessagesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MessagesViewHolder messagesViewHolder, int i) {
        messagesViewHolder.messageTitle.setText(messages.get(i).getTable() + " " + messages.get(i).getSeat());
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateString = formatter.format(messages.get(i).getTimeStamp());
        messagesViewHolder.messageTime.setText(dateString);
        String icon = messages.get(i).getType();
        switch (icon) {
            case MessegesObj.MessageType.COME:
                messagesViewHolder.messageType.setImageResource(R.drawable.ic_directions_run_black_24dp);
                break;
            case MessegesObj.MessageType.FORK:
                messagesViewHolder.messageType.setImageResource(R.drawable.ic_restaurant_black_24dp);
                break;
            case MessegesObj.MessageType.TAXI:
                messagesViewHolder.messageType.setImageResource(R.drawable.ic_local_taxi_black_24dp);
                break;
            case MessegesObj.MessageType.CHECK:
                messagesViewHolder.messageType.setImageResource(R.drawable.ic_format_list_numbered_black_24dp);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessagesViewHolder extends RecyclerView.ViewHolder {
        TextView messageTitle;
        ImageView messageType;
        TextView messageTime;

        MessagesViewHolder(View itemView) {
            super(itemView);
            messageTitle = (TextView) itemView.findViewById(R.id.message_title);
            messageType = (ImageView) itemView.findViewById(R.id.message_type);
            messageTime = (TextView) itemView.findViewById(R.id.message_time);
        }
    }
}