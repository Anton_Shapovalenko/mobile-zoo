package com.example.anton.mobilezoo.data;

/**
 * Created by anton on 21.04.2016.
 */
public class MenuItemObj {
    public static int id;
    public static String name;
    public static double price;
    public static String description;

    public MenuItemObj(int id, String name, double price, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        MenuItemObj.id = id;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        MenuItemObj.name = name;
    }

    public static double getPrice() {
        return price;
    }

    public static void setPrice(double price) {
        MenuItemObj.price = price;
    }

    public static String getDescription() {
        return description;
    }

    public static void setDescription(String description) {
        MenuItemObj.description = description;
    }
}
