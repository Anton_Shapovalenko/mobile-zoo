package com.example.anton.mobilezoo.db;

/**
 * Created by anton on 20.04.2016.
 */
public class TableOrder {

    public static final String TABLE_NAME = "order_table";

    public static final String _ID = "order_id";
    public static final String TABLE = "order_table";
    public static final String SEAT = "seat";
    public static final String TOTAL = "total";
    public static final String STATUS = "status";

    public static final String MESSAGES = "Messages";
    public static final String PAYFOR = "pay_for";


    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TABLE + " VARCHAR(255),"
            + SEAT + " VARCHAR(255),"
            + TOTAL + " VARCHAR(255),"
            + STATUS + " INT,"

            + PAYFOR + " VARCHAR(255),"
            + MESSAGES + " VARCHAR(255));";

    static final String AUTHORITY = "com.example.anton.Mobile_zoo.DB.TableOrder";


}
