package com.example.anton.mobilezoo.ui.fragments;

/**
 * Created by anton on 13.04.2016.
 */

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anton.mobilezoo.InformationActivity;
import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.MessegesObj;
import com.example.anton.mobilezoo.db.DB;
import com.example.anton.mobilezoo.db.InsertDB;
import com.example.anton.mobilezoo.db.Messeges;
import com.example.anton.mobilezoo.ui.BasicActivity;
import com.example.anton.mobilezoo.ui.list.MessagesAdapter;
import com.example.anton.mobilezoo.ui.list.RecyclerItemClickListener;

import java.util.ArrayList;


public class TabList extends Fragment {

    private ArrayList<MessegesObj> table;
    private ArrayList<MessegesObj> tableFromDB = new ArrayList<>();
    private RecyclerView rv;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_list, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);



        final DB sqh = ((BasicActivity)getActivity()).getDb();
        SQLiteDatabase sqdb = sqh.getReadableDatabase();

        Cursor cursor = getTablesCursor(sqdb);

        if (cursor.getCount() == 0) {
            sqh.insertMessegeseData(table);
            InsertDB.fillingDB(sqh);

        }

        while (cursor.moveToNext()) {
            int tableTable = cursor.getInt(cursor.getColumnIndex(Messeges.TABLE));
            String seat = cursor.getString(cursor.getColumnIndex(Messeges.SEAT));
            String type = cursor.getString(cursor.getColumnIndex(Messeges.TYPE));
            long timeStamp = cursor.getLong(cursor.getColumnIndex(Messeges.TIMESTAMP));
            MessegesObj newMess = new MessegesObj(tableTable, seat, type, timeStamp);
            tableFromDB.add(newMess);
        }
        cursor.close();


        rv = (RecyclerView) v.findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        initializeAdapter();


        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        MessegesObj obj = tableFromDB.get(position);
                        Intent intent = new Intent(getActivity(), InformationActivity.class);
                        intent.putExtra(InformationActivity.EXTRA_KEY_MESSAGEOBJ, obj);
                        Log.d("TAG", "startActivity");
                        startActivity(intent);
                    }
                })
        );

    }

    private Cursor getTablesCursor(SQLiteDatabase sqdb) {
        return sqdb.query(Messeges.TABLE_NAME, new String[]{Messeges._ID,
                        Messeges.TABLE, Messeges.SEAT, Messeges.TYPE, Messeges.TIMESTAMP},
                null, null, null, null, null);
    }


    public void setLText(String test) {
        String test1 = test;
    }



    private void initializeAdapter() {
        MessagesAdapter adapter = new MessagesAdapter(tableFromDB);
        rv.setAdapter(adapter);
    }

}