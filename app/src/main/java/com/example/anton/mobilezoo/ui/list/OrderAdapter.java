package com.example.anton.mobilezoo.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anton.mobilezoo.R;
import com.example.anton.mobilezoo.data.OrderItemObj;

import java.util.List;

/**
 * Created by anton on 26.04.2016.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderItemViewHolder> {


    List<OrderItemObj> orders;

    public OrderAdapter(List<OrderItemObj> table) {
        this.orders = table;
    }


    @Override
    public OrderItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.oreder_item, viewGroup, false);
        return new OrderItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderItemViewHolder orderViewHolder, int i) {
        orderViewHolder.orderName.setText(orders.get(i).getName());
        orderViewHolder.orderPrice.setText("$" + String.format("%.2f", orders.get(i).getPrice()));
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class OrderItemViewHolder extends RecyclerView.ViewHolder {
        TextView orderName;
        TextView orderPrice;

        OrderItemViewHolder(View itemView) {
            super(itemView);
            orderName = (TextView) itemView.findViewById(R.id.order_name);
            orderPrice = (TextView) itemView.findViewById(R.id.order_price);
        }
    }
}