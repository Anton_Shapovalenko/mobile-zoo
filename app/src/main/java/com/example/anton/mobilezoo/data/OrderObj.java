package com.example.anton.mobilezoo.data;

import java.util.ArrayList;

/**
 * Created by anton on 21.04.2016.
 */
public class OrderObj {
    public int table;
    public String seat;
    public int total;
    public String status;
    public String messages;
    public ArrayList<OrderItemObj> listOrderItemObj;

    public OrderObj(int table, String seat, int total, String status, String messages, ArrayList<OrderItemObj> listOrderItemObj) {
        this.table = table;
        this.seat = seat;
        this.total = total;
        this.status = status;
        this.messages = messages;
        this.listOrderItemObj = listOrderItemObj;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public ArrayList<OrderItemObj> getListOrderItemObjs() {
        return listOrderItemObj;
    }

    public void setListOrderItemObjs(ArrayList<OrderItemObj> listOrderItemObj) {
        this.listOrderItemObj = listOrderItemObj;
    }
}
